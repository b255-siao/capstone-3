import './App.css';

import { Container } from 'react-bootstrap'
import { BrowserRouter as Router } from 'react-router-dom'
import { Route, Routes } from 'react-router-dom'
import { useState, useEffect } from 'react'

import Register from './pages/Register.js'
import Login from './pages/Login.js'
import Dashboard from './pages/admin/Dashboard'
import AppNavBar from './components/AppNavBar'

import ProductCreate from './pages/admin/ProductCreate'
import ProductDetails from './pages/admin/ProductDetails'
import ProductEdit from './pages/admin/ProductEdit'

import { UserProvider } from './UserContext'

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect(() => {
    console.log(user)
    console.log(localStorage)
  }, [user])

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <Container fluid>
        <AppNavBar></AppNavBar>
          <Routes>
            <Route path="/register" element={<Register/>}/>
            <Route path="/login" element={<Login/>}/>
            <Route path="/dashboard" element={<Dashboard />}/>
            <Route path="/dashboard/admin/create" element={<ProductCreate/>} />
            <Route path="/admin/detail/:product_id" element={<ProductDetails/>} />
            <Route path="/admin/edit/:product_id" element={<ProductEdit/>} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  )
}

export default App;
