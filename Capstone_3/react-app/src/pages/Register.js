import { Form, Button } from 'react-bootstrap'
import { useState, useEffect, useContext } from 'react'
import { useNavigate, Navigate } from 'react-router-dom'
import UserContext from '../UserContext'


export default function Register() {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [confirmPass, setConfirmPass] = useState('')
  const [isActive, setIsActive] = useState('')

  const navigate = useNavigate()

  console.log(email)
  console.log(password)
  console.log(confirmPass)
  console.log(process.env.REACT_APP_API_URL)

  const {user} = useContext(UserContext)

  function registerUser(e) {
    e.preventDefault()

    fetch(`http://localhost:5000/api/v1/users/checkEmail`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)

      if(data === true) {
        console.log('Duplicate email found')
      } else {
        fetch(`http://localhost:5000/api/v1/users/register`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            email: email,
            password: password
          })
        })
        .then(res => res.json())
        .then(data => {
          console.log(data)
          if(data === true) {
            setEmail('')
            setPassword('')
            setConfirmPass('')

            navigate('/login')
          } else {
            console.log('Something went wrong')
          }
        })
      }
    })
  }

  useEffect(() => {
    if (
      (email !== '' && password !== '' && confirmPass !== '') && 
      (password === confirmPass)
    ) {
      setIsActive(true)
    } else {
      setIsActive(false)
    }
  }, [email, password, confirmPass])

  return(
    (user.id !== null) ?
      <Navigate to="/"/>
    :
    <Form
      onSubmit={(e) => registerUser(e)}
      style={{
        alignItems: 'center',
        justifyContent: 'center'
      }}
      className="p-4 me-5 ms-5 fluid-container"
    >
      <h1 style={{textAlign: 'center'}}>You must Sign Up to join</h1>
      <Form.Group className="mt-3" controlId="email">
        <Form.Label className="label">Email Address</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter your email"
          value={email}
          onChange={e => setEmail(e.target.value)}
          required
        ></Form.Control>
      </Form.Group>

      <Form.Group className="mt-3" controlId="password">
        <Form.Label className="label">Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Enter your password"
          value={password}
          onChange={e => setPassword(e.target.value)}
          required
        ></Form.Control>
      </Form.Group>

      <Form.Group className="mt-3" controlId="confirmPass">
        <Form.Label className="label">Confirm Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Confirm your password"
          value={confirmPass}
          onChange={e => setConfirmPass(e.target.value)}
          required
        ></Form.Control>
      </Form.Group>

      {
        isActive ?
          <Button className="mt-4" type="submit" size="md" id="submitBtn">Sign Up</Button>
        :
          <Button className="mt-4" size="md" type="submit" id="submitBtn" disabled>Sign Up</Button>
      }
      <div className="d-grid gap-2">
      </div>
    </Form>
  )
}