import { useParams, Link, useNavigate } from 'react-router-dom'
import { useEffect, useState } from 'react'

const ProductEdit = () => {
  const {productId} = useParams()

  const [name, nameChange] = useState("")
  const [description, descriptionChange] = useState("")
  const [price, priceChange] = useState(null)
  // const [validation, valChange] = useState(false)
  // const [availability, availabilityChange] = useState("")
  // const [is_active, isActiveChange] = useState(true)
  const navigate = useNavigate()

  const handleSubmit = (e) => {
    e.preventDefault();
    // console.log({name, price, availability, is_active})
    const productData = {name, price, description}
    // , availability, is_active

    fetch("http://localhost:5000/api/v1/products/createProduct", {
      method: 'POST',
      headers: {
        "content-type": "application/json",
      },
      body: JSON.stringify(productData)
    })
    .then((res) => {
      alert('Saved successfully.')
      navigate('/dashboard')
    })
    .catch((err) => {
      alert(err.message)
      // console.log(err.message)
      navigate('/dashboard')
    })
  }

  // const [productData, productDataChange] = useState({})

  useEffect(() => {
    fetch("http://localhost:5000/api/v1/products/retrieveAllProducts"+productId)
      .then((res) => {
        return res.json()
      })
      .then((res) => {
        console.log('products', res)
        // console.log('productData', productData)
        // productDataChange(res)
      })
      .catch((err) => {
        console.log(err.message)
      })
  }, [])
  return (
    <div>
      <div className="row mt-5">
        <div className="offset-lg-3 col-lg-6">
          <form className="container" onSubmit={handleSubmit}>
            <div className="card" style={{textAlign: 'left'}}>
              {/* <div className="card-title">
                <h2>Create Product</h2>
              </div> */}
              <div className="card-body">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="form-group mb-1">
                      <label className='mb-2'>Name</label>
                      <input 
                        onChange={(e) => nameChange(e.target.value)}   
                        value={name} 
                        className="form-control"
                      ></input>
                        {/* onMouseDown={(e) => valChange(true)}  */}
                      {/* {name.length===0 && validation && <span className="text-danger">Enter the name</span>} */}
                    </div>
                  </div>

                  <div className="col-lg-12 mt-3">
                    <div className="form-group">
                      <label className='mb-2'>Description</label>
                      <input
                       required
                       onChange={(e) => descriptionChange(e.target.value)} 
                       value={description}
                       className="form-control"
                      ></input>
                       {/* onMouseDown={(e) => valChange(true)}  */}
                      {/* {description.length===0 && validation && <span className="text-danger">Enter the description</span>} */}
                    </div>
                  </div>

                  <div className="col-lg-12 mt-3">
                    <div className="form-group">
                      <label className='mb-2'>Price</label>
                      <input type="number" required onChange={(e) => priceChange(e.target.value)} value={price} className="form-control"></input>
                    </div>
                  </div>

                  {/* <div className="col-lg-12 mt-3">  
                    <div className="form-group">
                      <label className='mb-2'>Availability</label>
                      <input onChange={(e) => availabilityChange(e.target.value)} value={availability} className="form-control"></input>
                    </div>
                  </div> */}

                  {/* <div className="col-lg-12 mt-3 mb-3">
                    <div className="form-check">
                      <input checked={is_active} onChange={(e) => isActiveChange(e.target.checked)} type="checkbox" className="form-check-input"></input>
                      <label value={is_active} className="form-check-label">Is Active</label>
                    </div>
                  </div> */}

                  <div className="col-lg-12 mt-3">
                    <div className="form-group">
                      <button className="btn btn-success me-2" type="submit">Save</button>
                      <Link to="/dashboard" className="btn btn-danger">Back</Link>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  )
}

export default ProductEdit