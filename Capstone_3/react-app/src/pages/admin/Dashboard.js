import { Table } from 'react-bootstrap'
// , Button
import { useEffect, useState } from 'react'
// Fragment, useContext
import { Link, useNavigate } from 'react-router-dom'

export default function Dashboard() {
  const [productData, productDataChange] = useState(null)

  const navigate = useNavigate()

  const LoadDetail = (id) => {
    navigate("/admin/detail/" + id)
  }

  const LoadEdit = (id) => {
    navigate("/admin/detail/" + id)
  }

  const RemoveFunction = (id) => {
    
  }
  
  useEffect(() => {
    fetch("http://localhost:5000/api/v1/products/retrieveAllProducts")
      .then((res) => {
        return res.json()
      })
      .then((res) => {
        console.log('products', res)
        // console.log('productData', productData)
        productDataChange(res)
      })
      .catch((err) => {
        console.log(err.message)
      })
  }, [])
  
  return (
    <div style={{alignItems: 'center'}}>
      <h1
        style={{textAlign: 'center'}}
        className="mt-4"
      >
        Admin Dashboard
      </h1>
      <div style={{textAlign: 'center'}}>
        <Link to="/dashboard/admin/create" className="btn btn-primary me-3">Add New Product</Link>
        <Link className="btn btn-success">Show User Orders</Link>
      </div>
        <Table
          bordered
          size="sm"
          responsive="lg"
          style={{margin: 'auto', display: 'inline-block'}}
          className="me-5 ms-5 mt-4 w-75"
        >
          <thead>
            <tr>
              <th>Name</th>
              <th>Description</th>
              <th>Price</th>
              {/* <th>Availability</th> */}
              <th>Is Active?</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            { 
              productData &&
              productData.map((item) => (
                <tr
                  key={item.id}
                >
                  <td>{item.name}</td>
                  <td>{item.description}</td>
                  <td>{item.price}</td>
                  {/* <td>{item.availability}</td> */}
                  <td>{item.is_active}</td>
                  <td>
                    <a href="/" onClick={() => {LoadEdit(item.id)}} className="btn btn-success me-2">Edit</a>
                    <a href="/" onClick={() => {RemoveFunction(item.id)}} className="btn btn-danger me-2">Remove</a>
                    <a href="/" onClick={() => {LoadDetail(item.id)}} className="btn btn-primary">Details</a>
                  </td>
                </tr>
              ))
            }

            {/* <tr>
              <td>Sherlock Holmes</td>
              <td>Open your door to the world of grilling with the sleek Spirit II E-210 gas grill. This two burner grill is built to fit small spaces, and packed with features such as the powerful GS4 grilling system, iGrill capability, and convenient side tables for placing serving trays. Welcome to the Weber family.</td>
              <td>$100.00</td>
              <td>100</td>
              <td>
                <Button className="me-2" variant="primary">Update</Button>
                <Button variant="danger">Disable</Button>
              </td>
            </tr>
            <tr>
              <td>Bilbo Baggins</td>
              <td>Open your door to the world of grilling with the sleek Spirit II E-210 gas grill. This two burner grill is built to fit small spaces, and packed with features such as the powerful GS4 grilling system, iGrill capability, and convenient side tables for placing serving trays. Welcome to the Weber family.</td>
              <td>$100.00</td>
              <td>100</td>
              <td>
                <Button className="me-2" variant="primary">Update</Button>
                <Button variant="danger">Disable</Button>
              </td>
            </tr>
            <tr>
              <td>Hermione Granger</td>
              <td>Open your door to the world of grilling with the sleek Spirit II E-210 gas grill. This two burner grill is built to fit small spaces, and packed with features such as the powerful GS4 grilling system, iGrill capability, and convenient side tables for placing serving trays. Welcome to the Weber family.</td>
              <td>$100.00</td>
              <td>100</td>
              <td>
                <Button className="me-2" variant="primary">Update</Button>
                <Button variant="danger">Disable</Button>
              </td>
            </tr> */}
          </tbody>
        </Table>
    </div>
  )
}