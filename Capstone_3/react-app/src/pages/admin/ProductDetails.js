import { useParams, Link } from 'react-router-dom'
import { useEffect, useState } from 'react'

const ProductDetails = () => {
  const {productId} = useParams()

  const [productData, productDataChange] = useState({})

  useEffect(() => {
    fetch("http://localhost:5000/api/v1/products/retrieveAllProducts"+productId)
      .then((res) => {
        return res.json()
      })
      .then((res) => {
        console.log('products', res)
        // console.log('productData', productData)
        productDataChange(res)
      })
      .catch((err) => {
        console.log(err.message)
      })
  }, [])

  return (
    <div>
      <div className="card" style={{textAlign: 'left'}}>
        <div className='card-title'>
          <h2>Product Details</h2>
        </div>
      </div>
      {
        productData &&
        <div>
          <h1>The product name is: {productData.name} ({productData.id})</h1>

          <h3>Description:</h3>
          <h5>{productData.description}</h5>
          <h3>Price</h3>
          <h5>{productData.price}</h5>
          <Link className='btn btn-danger' to="/">Back To Listing</Link>
        </div>
      }
    </div>
  )
}

export default ProductDetails