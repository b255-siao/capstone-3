import { useState, useEffect, useContext } from 'react'
import { Form, Button } from 'react-bootstrap'
import { Navigate } from 'react-router-dom'
import userContext from '../UserContext'

export default function Login(props) {
  const { user, setUser } = useContext(userContext)

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [isActive, setIsActive] = useState(true)

  function authenticate(e) {
    e.preventDefault()

    fetch(`http://localhost:5000/api/v1/users/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)

      if(typeof data.access !== "undefined") {
        localStorage.setItem('token', data.access)
        retrieveUserDetails(data.access)
      }
    })
    setEmail('')
    setPassword('')
  }

  const retrieveUserDetails = (token) => {
    fetch(`http://localhost:5000/api/v1/users/details`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)

      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      })
    })
  }

  useEffect(() => {
    if (email !== '' && password !== '') {
      setIsActive(true)
    } else {
      setIsActive(false)
    }
  }, [email, password])

  return(
    (user.id !== null) ?
    <Navigate to="/register" />
    :
    <Form
      onSubmit={(e) => authenticate(e)}
      style={{
        alignItems: 'center',
        justifyContent: 'center'
      }}
      className="p-4 me-5 ms-5 fluid-container"
    >
      <h1 style={{textAlign: 'center'}}>Log in to your Account</h1>
      <Form.Group className="mt-3" controlId="userEmail">
        <Form.Label className="label">Email Address</Form.Label>
        <Form.Control
          type="email" 
          placeholder="Enter email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
        ></Form.Control>
      </Form.Group>

      <Form.Group className="mt-3" controlId="password">
        <Form.Label className="label">Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Enter your password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          required
        ></Form.Control>
      </Form.Group>

      { isActive ?
        <div className="d-grid gap-2">
          <Button 
            variant="primary" 
            type="submit" 
            id="submitBtn" 
            className="mt-4" 
            size="md"
          >Login</Button>
        </div>  
        :
        <div className="d-grid gap-2">
          <Button
            variant="primary" 
            type="submit" 
            id="submitBtn" 
            className="mt-4" 
            size="md"
            disabled
          >Login</Button>
        </div>
      }
      <div className="mt-2">
        <p>Don't have an account yet? <span style={{color: "blue"}}>Click here</span> to register</p>
      </div>
    </Form>
  )
}