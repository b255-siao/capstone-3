import Container from 'react-bootstrap/Container'

import React from 'react'
import { useContext } from 'react'
import {Link, NavLink} from 'react-router-dom'

import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'

import UserContext from '../UserContext'

export default function AppNavBar() {
  const { user } = useContext(UserContext)

  return(
    <Navbar bg="light" expand="lg">
      <Container fluid>
        <Navbar.Brand as={Link} to="/">Xtyle</Navbar.Brand>
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            {/* <Nav.Link as={NavLink} to="/">Home</Nav.Link> */}
            {/* <Nav.Link as={NavLink} to="/products" exact>Products</Nav.Link> */}
            <Nav.Link as={NavLink} to="/dashboard">Dashboard</Nav.Link>
            {
              (user.id !== null)
              ?
              {/* <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link> */}
              :
              <React.Fragment>
                <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
              </React.Fragment>
            }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}